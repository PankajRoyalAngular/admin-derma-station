export const environment = {
    production: true,
    apiUrl: 'https://dermaquestapi.azurewebsites.net/api',
    rootUrl: 'https://dermaquestapi.azurewebsites.net/'
};
