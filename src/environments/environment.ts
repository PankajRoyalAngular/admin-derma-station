export const environment = {
    production: false,
    apiUrl: 'https://dermaquestapi.azurewebsites.net/api',
    rootUrl: 'https://dermaquestapi.azurewebsites.net/'
};
