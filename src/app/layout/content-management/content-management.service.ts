import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { map } from 'rxjs/operators';
import { ApiEndPoint } from '../../shared/enum/api-end-point.enum';
import { environment } from '../../../environments/environment';

@Injectable({
    providedIn: 'root'
})
export class ContentManagementService {
    constructor(private http: HttpClient) {}

    getBlogList(data: any) {
        return this.http.post<any>(environment.apiUrl + '/' + ApiEndPoint.getBlogsList, data);
    }

    getBlogDetails(blogId: any) {
        return this.http.get<any>(environment.apiUrl + '/' + ApiEndPoint.getBlogDetails + '?blogId=' + blogId);
    }

    deleteBlog(data: any) {
        return this.http.delete<any>(environment.apiUrl + '/' + ApiEndPoint.deleteBlog + '?blogId=' + data);
    }
}
