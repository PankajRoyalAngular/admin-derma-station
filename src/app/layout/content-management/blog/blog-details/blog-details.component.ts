import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators, FormBuilder, FormArray } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';
import { ContentManagementService } from '../../content-management.service';

@Component({
    selector: 'app-blog-details',
    templateUrl: './blog-details.component.html',
    styleUrls: ['./blog-details.component.css']
})
export class BlogDetailsComponent implements OnInit {
    blogInfoForm: FormGroup;
    blogDetails: any;
    blogId: any;
    isLoaded = false;
    rootUrl: any;
    blogImage: any;

    constructor(
        private formBuilder: FormBuilder,
        private route: ActivatedRoute,
        private contentService: ContentManagementService,
        private spinner: NgxSpinnerService
    ) {}

    ngOnInit(): void {
        this.blogId = this.route.snapshot.paramMap.get('id');
        this.blogInfoForm = this.formBuilder.group({
            id: new FormControl({ value: '', disabled: true }, Validators.required),
            title: new FormControl({ value: '', disabled: true }, Validators.required),
            titleInAr: new FormControl({ value: '', disabled: true }, Validators.required),
            authorName: new FormControl({ value: '', disabled: true }, Validators.required),
            authorNameInAr: new FormControl({ value: '', disabled: true }, Validators.required),
            description: new FormControl({ value: '', disabled: true }, Validators.required),
            descriptionInAr: new FormControl({ value: '', disabled: true }, Validators.required),
            createdDate: new FormControl({ value: '', disabled: true }, Validators.required),
            doctorName: new FormControl({ value: '', disabled: true }, Validators.required),
            isAdminApproved: new FormControl(''),
            employees: new FormArray([new FormControl(), new FormControl(), new FormControl()])
        });
        this.getBlogDetails();
        this.setEmployeeValues();
    }

    get employees(): FormArray {
        return this.blogInfoForm.get('employees') as FormArray;
    }
    setEmployeeValues() {
        this.employees.patchValue(['111', '112', '231']);
    }

    getBlogDetails() {
        this.spinner.show();
        this.contentService.getBlogDetails(this.blogId).subscribe((res) => {
            this.blogDetails = res.data;
            this.blogInfoForm.controls['id'].setValue(res.data.id);
            this.blogInfoForm.controls['title'].setValue(res.data.title);
            this.blogInfoForm.controls['titleInAr'].setValue(res.data.titleInAr);
            this.blogInfoForm.controls['authorName'].setValue(res.data.authorName);
            this.blogInfoForm.controls['authorNameInAr'].setValue(res.data.authorNameInAr);
            this.blogInfoForm.controls['description'].setValue(res.data.description);
            this.blogInfoForm.controls['descriptionInAr'].setValue(res.data.descriptionInAr);
            this.blogInfoForm.controls['createdDate'].setValue(res.data.createdDate);
            this.spinner.hide();
        });
    }
    onSubmit() {
        console.log(this.blogInfoForm.value);
    }
}
