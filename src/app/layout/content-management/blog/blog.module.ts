import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { BlogRoutingModule } from './blog-routing.module';
import { BlogComponent } from './blog.component';
import { BlogListComponent } from './blog-list/blog-list.component';
import { PageHeaderModule } from '../../../shared/modules/page-header/page-header.module';
import { BlogDetailsComponent } from './blog-details/blog-details.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

@NgModule({
    declarations: [BlogComponent, BlogListComponent, BlogDetailsComponent],
    imports: [CommonModule, BlogRoutingModule, PageHeaderModule, ReactiveFormsModule, FormsModule]
})
export class BlogModule {}
