import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';
import { ToastrService } from 'ngx-toastr';
import Swal from 'sweetalert2';
import { environment } from '../../../../../environments/environment';
import { Filter } from '../../../../shared/models/filter';
import { ContentManagementService } from '../../content-management.service';

@Component({
    selector: 'app-blog-list',
    templateUrl: './blog-list.component.html',
    styleUrls: ['./blog-list.component.css']
})
export class BlogListComponent implements OnInit {
    pageNumber: any = 1;
    pageSize: any = 10;
    rootUrl: any = environment.rootUrl;
    blogsList: any;
    blogId: any;
    constructor(
        private contentService: ContentManagementService,
        private router: Router,
        private spinner: NgxSpinnerService,
        private toster: ToastrService
    ) {}

    ngOnInit(): void {
        this.getBlogList();
    }

    getBlogList() {
        var data = {
            sortOrder: '',
            sortField: '',
            pageNumber: this.pageNumber,
            pageSize: this.pageSize,
            searchQuery: '',
            filterBy: ''
        };

        this.contentService.getBlogList(data).subscribe((res) => {
            this.blogsList = res.data.dataList;
            this.spinner.hide();

            console.log(this.blogsList);
        });
    }

    edit(id) {}

    details(id) {
        this.router.navigate(['./content-management/blog-list', id]);
    }

    delete(id) {
        this.blogId = id;
        Swal.fire({
            title: 'Delete Request',
            html: 'Are you sure you want to delete this blog',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes',
            cancelButtonText: 'No'
        }).then((result) => {
            if (result.isConfirmed) {
                this.spinner.show();
                this.contentService.deleteBlog(this.blogId).subscribe((res) => {
                    if (res.status == true) {
                        this.getBlogList();
                        this.toster.success(res.message);
                        this.spinner.hide();
                    } else {
                        this.toster.warning(res.message);
                        this.spinner.hide();
                    }
                });
            }
        });
    }
}
