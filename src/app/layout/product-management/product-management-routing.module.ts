import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
    {
        path: '',
        loadChildren: () => import('./brand/brand.module').then((m) => m.BrandModule)
    },
    {
        path: '',
        loadChildren: () => import('./ingredients/ingredients.module').then((m) => m.IngredientsModule)
    },
    {
        path: '',
        loadChildren: () => import('./concerns/concerns.module').then((m) => m.ConcernsModule)
    },
    {
        path: '',

        loadChildren: () => import('./skin/skin.module').then((m) => m.SkinModule)
    },
    {
        path: '',
        loadChildren: () => import('./category/category.module').then((m) => m.CategoryModule)
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class ProductManagementRoutingModule {}
