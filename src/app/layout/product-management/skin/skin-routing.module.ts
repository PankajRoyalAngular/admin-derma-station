import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { SkinAllTypesComponent } from './skin-all-types/skin-all-types.component';
import { SkinComponent } from './skin.component';

const routes: Routes = [
    {
        path: 'product-management',
        component: SkinComponent,
        children: [
            {
                path: 'skintypes',
                component: SkinAllTypesComponent
            }
        ]
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class SkinRoutingModule {}
