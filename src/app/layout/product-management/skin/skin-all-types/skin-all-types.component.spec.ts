import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SkinAllTypesComponent } from './skin-all-types.component';

describe('SkinAllTypesComponent', () => {
  let component: SkinAllTypesComponent;
  let fixture: ComponentFixture<SkinAllTypesComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SkinAllTypesComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SkinAllTypesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
