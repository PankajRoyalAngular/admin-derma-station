import { Component, OnInit } from '@angular/core';
import { NgxSpinnerService } from 'ngx-spinner';
import { ToastrService } from 'ngx-toastr';
import Swal from 'sweetalert2';
import { ProductManagementService } from '../../product-management.service';
import { routerTransition } from '../../../../router.animations';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ModalDismissReasons, NgbModal, NgbModalConfig } from '@ng-bootstrap/ng-bootstrap';

@Component({
    selector: 'app-skin-all-types',
    templateUrl: './skin-all-types.component.html',
    styleUrls: ['./skin-all-types.component.css'],
    animations: [routerTransition()]
})
export class SkinAllTypesComponent implements OnInit {
    skinTypesList: Array<any>;
    skinTypeId: any;
    skinTypeForm: FormGroup;
    closeResult = '';
    showId: boolean = false;

    constructor(
        private productService: ProductManagementService,
        private spinner: NgxSpinnerService,
        private toastr: ToastrService,
        private fb: FormBuilder,
        config: NgbModalConfig,
        private modalService: NgbModal
    ) {
        config.backdrop = 'static';
    }

    ngOnInit(): void {
        this.getAllSkinTypes();

        this.skinTypeForm = this.fb.group({
            skinTypeId: [0, Validators.required],
            skinTypeName: ['', Validators.required],
            skinTypeNameInA: ['', Validators.required],
            showInTopMenu: ['', Validators.required]
        });
    }

    getAllSkinTypes() {
        this.spinner.show();
        this.productService.allSkinTypes().subscribe((res) => {
            this.skinTypesList = res.data;
            this.spinner.hide();
        });
    }

    onDelete(data) {
        this.skinTypeId = data.skinTypeId;

        Swal.fire({
            title: 'Delete Request',
            html: 'Are you sure you want to delete this Skin type',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes',
            cancelButtonText: 'No'
        }).then((result) => {
            if (result.isConfirmed) {
                this.spinner.show();
                this.productService.deleteSkinTypes(this.skinTypeId).subscribe((res) => {
                    this.getAllSkinTypes();
                    if (res.status == true) {
                        this.toastr.success(res.message);
                    } else {
                        this.toastr.warning(res.message);
                    }
                    this.spinner.hide();
                });
            }
        });
    }
    open(content, item) {
        if (item !== 'add') {
            this.showId = true;
            this.skinTypeForm.controls['skinTypeId'].setValue(item.skinTypeId);
            this.skinTypeForm.controls['skinTypeName'].setValue(item.skinTypeName);
            this.skinTypeForm.controls['skinTypeNameInA'].setValue(item.skinTypeNameInA);
            this.skinTypeForm.controls['showInTopMenu'].setValue(item.showInTopMenu);
        } else {
            this.skinTypeForm.reset();
            this.showId = false;
            this.skinTypeForm.controls['skinTypeId'].setValue(0);
        }
        this.modalService.open(content, { ariaLabelledBy: 'modal-basic-title' }).result.then(
            (result) => {
                this.closeResult = `Closed with: ${result}`;
            },
            (reason) => {
                this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
            }
        );
    }

    private getDismissReason(reason: any): string {
        if (reason === ModalDismissReasons.ESC) {
            return 'by pressing ESC';
        } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
            return 'by clicking on a backdrop';
        } else {
            return `with: ${reason}`;
        }
    }

    addSkinType() {
        if (this.skinTypeForm.invalid) {
            return;
        }
        this.spinner.show();
        this.productService.addSkinType(this.skinTypeForm.value).subscribe((res) => {
            if (res.status) {
                this.toastr.success(res.message);
                this.getAllSkinTypes();
                this.modalService.dismissAll();
                this.skinTypeForm.reset();
                this.spinner.hide();
            } else {
                this.spinner.hide();
                this.toastr.warning(res.message);
            }
        });
    }
}
