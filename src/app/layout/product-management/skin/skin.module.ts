import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SkinRoutingModule } from './skin-routing.module';
import { SkinAllTypesComponent } from './skin-all-types/skin-all-types.component';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import { PageHeaderModule } from '../../../shared/modules/page-header/page-header.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SharedModule } from '../../../shared/shared.module';

@NgModule({
    declarations: [SkinAllTypesComponent],
    imports: [
        CommonModule,
        SkinRoutingModule,
        NgxDatatableModule,
        PageHeaderModule,
        ReactiveFormsModule,
        FormsModule,
        SharedModule
    ]
})
export class SkinModule {}
