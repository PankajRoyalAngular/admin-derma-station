import { SafeMethodCall } from '@angular/compiler';
import { AfterViewInit, ChangeDetectorRef, Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { ModalDismissReasons, NgbModal, NgbModalConfig } from '@ng-bootstrap/ng-bootstrap';
import { NgxSpinnerService } from 'ngx-spinner';
import { ToastrService } from 'ngx-toastr';
import Swal from 'sweetalert2';
import { routerTransition } from '../../../../router.animations';
import { ProductManagementService } from '../../product-management.service';
@Component({
    selector: 'app-ingredients-all-list',
    templateUrl: './ingredients-all-list.component.html',
    styleUrls: ['./ingredients-all-list.component.css'],
    animations: [routerTransition()]
})
export class IngredientsAllListComponent implements OnInit {
    ingredientsList: Array<any>;
    ingredientId: any;
    closeResult: any;
    showId: boolean = false;
    addIngredintsForm: FormGroup;
    constructor(
        private productService: ProductManagementService,
        private spinner: NgxSpinnerService,
        private toastr: ToastrService,
        private modalService: NgbModal,
        private fb: FormBuilder,
        private toster: ToastrService,
        config: NgbModalConfig,
        private cdref: ChangeDetectorRef
    ) {
        config.backdrop = 'static';
    }

    ngOnInit(): void {
        this.getAllIngredients();

        this.addIngredintsForm = this.fb.group({
            ingredientId: [0, Validators.required],
            ingredientName: ['', Validators.required],
            benefits: ['', Validators.required],
            ingredientNameInAr: ['', Validators.required],
            benefitsInAr: ['', Validators.required],
            showInTopMenu: ['', Validators.required]
        });
    }

    getAllIngredients() {
        this.spinner.show();
        this.productService.allIngredients().subscribe((res) => {
            this.ingredientsList = res.data;
            this.spinner.hide();
        });
    }

    onDelete(data) {
        this.ingredientId = data.ingredientId;

        Swal.fire({
            title: 'Ingredients Request',
            html: 'Are you sure you want to delete this Ingredients',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes',
            cancelButtonText: 'No'
        }).then((result) => {
            if (result.isConfirmed) {
                this.spinner.show();
                this.productService.deleteIngredients(this.ingredientId).subscribe((res) => {
                    this.getAllIngredients();
                    if (res.status == true) {
                        this.toastr.success(res.message);
                    } else {
                        this.toastr.warning(res.message);
                    }
                    this.spinner.hide();
                });
            }
        });
    }

    open(content, item) {
        if (item !== 'add') {
            this.showId = true;
            this.addIngredintsForm.controls['ingredientId'].setValue(item.ingredientId);
            this.addIngredintsForm.controls['ingredientName'].setValue(item.ingredientName);
            this.addIngredintsForm.controls['ingredientNameInAr'].setValue(item.ingredientNameInAr);
            this.addIngredintsForm.controls['benefits'].setValue(item.benefits);
            this.addIngredintsForm.controls['benefitsInAr'].setValue(item.benefitsInAr);
            this.addIngredintsForm.controls['showInTopMenu'].setValue(item.showInTopMenu);
        } else {
            this.addIngredintsForm.reset();
            this.showId = false;
            this.addIngredintsForm.controls['ingredientId'].setValue(0);
        }

        this.modalService.open(content, { ariaLabelledBy: 'modal-basic-title' }).result.then(
            (result) => {
                this.closeResult = `Closed with: ${result}`;
            },
            (reason) => {
                this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
            }
        );
    }

    private getDismissReason(reason: any): string {
        if (reason === ModalDismissReasons.ESC) {
            return 'by pressing ESC';
        } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
            return 'by clicking on a backdrop';
        } else {
            return `with: ${reason}`;
        }
    }

    onAddIngredients() {
        console.log(this.addIngredintsForm.value);
        if (this.addIngredintsForm.invalid) {
            return;
        }
        this.spinner.show();
        this.productService.addUpdateIngredients(this.addIngredintsForm.value).subscribe((res) => {
            if (res.status) {
                this.toster.success(res.message);
                this.getAllIngredients();
                this.modalService.dismissAll();
                this.addIngredintsForm.reset();
                this.spinner.hide();
            } else {
                this.spinner.hide();
                this.toster.warning(res.message);
            }
        });
    }
}
