import { ComponentFixture, TestBed } from '@angular/core/testing';

import { IngredientsAllListComponent } from './ingredients-all-list.component';

describe('IngredientsAllListComponent', () => {
  let component: IngredientsAllListComponent;
  let fixture: ComponentFixture<IngredientsAllListComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ IngredientsAllListComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(IngredientsAllListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
