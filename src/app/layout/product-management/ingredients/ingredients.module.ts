import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { IngredientsRoutingModule } from './ingredients-routing.module';
import { IngredientsAllListComponent } from './ingredients-all-list/ingredients-all-list.component';
import { PageHeaderModule } from '../../../shared/modules/page-header/page-header.module';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SharedModule } from '../../../shared/shared.module';

@NgModule({
    declarations: [IngredientsAllListComponent],
    imports: [
        CommonModule,
        IngredientsRoutingModule,
        PageHeaderModule,
        NgxDatatableModule,
        ReactiveFormsModule,
        FormsModule,
        SharedModule
    ]
})
export class IngredientsModule {}
