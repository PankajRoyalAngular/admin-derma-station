import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { IngredientsAllListComponent } from './ingredients-all-list/ingredients-all-list.component';
import { IngredientsComponent } from './ingredients.component';

const routes: Routes = [
    {
        path: 'product-management',
        component: IngredientsComponent,
        children: [
            {
                path: 'ingredients',
                component: IngredientsAllListComponent
            }
        ]
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class IngredientsRoutingModule {}
