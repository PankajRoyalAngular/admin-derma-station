import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ProductManagementRoutingModule } from './product-management-routing.module';
import { BrandAllListComponent } from './brand/brand-all-list/brand-all-list.component';
import { PageHeaderModule } from '../../shared/modules/page-header/page-header.module';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import { IngredientsComponent } from './ingredients/ingredients.component';
import { ProductManagementService } from './product-management.service';
import { SkinComponent } from './skin/skin.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ProductManagementComponent } from './product-management.component';

@NgModule({
    declarations: [BrandAllListComponent, IngredientsComponent, SkinComponent, ProductManagementComponent],
    imports: [
        CommonModule,
        ProductManagementRoutingModule,
        PageHeaderModule,
        NgxDatatableModule,
        ReactiveFormsModule,
        FormsModule
    ],
    providers: [ProductManagementService]
})
export class ProductManagementModule {}
