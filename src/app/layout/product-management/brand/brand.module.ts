import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { BrandRoutingModule } from './brand-routing.module';

import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import { BrandComponent } from './brand.component';
import { PageHeaderModule } from '../../../shared/modules/page-header/page-header.module';
import { SharedModule } from '../../../shared/shared.module';

@NgModule({
    declarations: [BrandComponent],
    imports: [CommonModule, BrandRoutingModule, NgxDatatableModule, PageHeaderModule, SharedModule]
})
export class BrandModule {}
