import { Component, Input, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ModalDismissReasons, NgbActiveModal, NgbModal, NgbModalConfig } from '@ng-bootstrap/ng-bootstrap';
import { NgxSpinnerService } from 'ngx-spinner';
import { ToastrService } from 'ngx-toastr';
import Swal from 'sweetalert2';
import { routerTransition } from '../../../../router.animations';
import { ProductManagementService } from '../../product-management.service';

@Component({
    selector: 'app-brand-all-list',
    templateUrl: './brand-all-list.component.html',
    styleUrls: ['./brand-all-list.component.css'],
    animations: [routerTransition()]
})
export class BrandAllListComponent implements OnInit {
    brandList: Array<any>;
    brandId: any;
    closeResult = '';
    addBrandForm: FormGroup;
    showId: boolean = false;

    constructor(
        private productManagementService: ProductManagementService,
        private spinner: NgxSpinnerService,
        private toster: ToastrService,
        private modalService: NgbModal,
        private fb: FormBuilder,
        config: NgbModalConfig
    ) {
        config.backdrop = 'static';
    }

    ngOnInit(): void {
        this.getAllBrandList();
        this.addBrandForm = this.fb.group({
            brandId: [0, Validators.required],
            brandName: ['', Validators.required],
            brandNameInAr: ['', Validators.required],
            isActive: [true, Validators.required],
            showInTopMenu: [false, Validators.required],
            isNewBrand: [false, Validators.required]
        });
    }

    open(content, item) {
        if (item !== 'add') {
            this.showId = true;
            this.addBrandForm.controls['brandId'].setValue(item.brandId);
            this.addBrandForm.controls['brandName'].setValue(item.brandName);
            this.addBrandForm.controls['brandNameInAr'].setValue(item.brandNameInAr);
            this.addBrandForm.controls['isActive'].setValue(item.isActive);
            this.addBrandForm.controls['showInTopMenu'].setValue(item.showInTopMenu);
            this.addBrandForm.controls['isNewBrand'].setValue(item.isNewBrand);
        } else {
            this.addBrandForm.reset();
            this.showId = false;
            this.addBrandForm.controls['brandId'].setValue(0);
        }

        this.modalService.open(content, { ariaLabelledBy: 'modal-basic-title' }).result.then(
            (result) => {
                this.closeResult = `Closed with: ${result}`;
            },
            (reason) => {
                this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
            }
        );
    }

    private getDismissReason(reason: any): string {
        if (reason === ModalDismissReasons.ESC) {
            return 'by pressing ESC';
        } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
            return 'by clicking on a backdrop';
        } else {
            return `with: ${reason}`;
        }
    }

    getAllBrandList() {
        this.spinner.show();
        this.productManagementService.allBrandList().subscribe((res) => {
            this.brandList = res.data.listAllBrands;
            this.spinner.hide();
        });
    }

    onDelete(data: any) {
        this.brandId = data.brandId;

        Swal.fire({
            title: 'Brand Request',
            html: 'Are you sure you want to delete this brand',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes',
            cancelButtonText: 'No'
        }).then((result) => {
            if (result.isConfirmed) {
                this.spinner.show();
                this.productManagementService.deleteBrand(this.brandId).subscribe((res) => {
                    this.getAllBrandList();

                    if (res.status == true) {
                        this.toster.success(res.message);
                    } else {
                        this.toster.warning(res.message);
                    }
                    this.spinner.hide();
                });
            }
        });
    }

    onAddUpdateBrand() {
        if (this.addBrandForm.invalid) {
            return;
        }
        this.spinner.show();
        this.productManagementService.onAddUpdateBrand(this.addBrandForm.value).subscribe((res) => {
            if (res.status) {
                this.toster.success(res.message);
                this.getAllBrandList();
                this.modalService.dismissAll();
                this.addBrandForm.reset();
                this.spinner.hide();
            } else {
                this.spinner.hide();
                this.toster.warning(res.message);
            }
        });
    }
}
