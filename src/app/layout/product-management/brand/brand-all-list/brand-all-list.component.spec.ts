import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BrandAllListComponent } from './brand-all-list.component';

describe('BrandAllListComponent', () => {
  let component: BrandAllListComponent;
  let fixture: ComponentFixture<BrandAllListComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BrandAllListComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BrandAllListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
