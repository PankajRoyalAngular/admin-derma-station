import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { BrandAllListComponent } from './brand-all-list/brand-all-list.component';
import { BrandComponent } from './brand.component';

const routes: Routes = [
    {
        path: 'product-management',
        component: BrandComponent,
        children: [
            {
                path: 'brand',
                component: BrandAllListComponent
            }
        ]
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class BrandRoutingModule {}
