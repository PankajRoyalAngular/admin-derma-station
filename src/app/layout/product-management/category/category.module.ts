import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CategoryRoutingModule } from './category-routing.module';
import { CategoryComponent } from './category.component';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import { PageHeaderModule } from '../../../shared/modules/page-header/page-header.module';
import { SharedModule } from '../../../shared/shared.module';
import { ProductCategoryComponent } from './product-category/product-category.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ProductSubCategoryComponent } from './product-sub-category/product-sub-category.component';
import { ProductSubSubCategoryComponent } from './product-sub-sub-category/product-sub-sub-category.component';

@NgModule({
    declarations: [CategoryComponent, ProductCategoryComponent, ProductSubCategoryComponent, ProductSubSubCategoryComponent],
    imports: [
        CommonModule,
        CategoryRoutingModule,
        NgxDatatableModule,
        PageHeaderModule,
        SharedModule,
        ReactiveFormsModule,
        FormsModule
    ]
})
export class CategoryModule {}
