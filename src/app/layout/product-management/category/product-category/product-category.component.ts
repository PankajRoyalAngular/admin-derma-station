import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { ModalDismissReasons, NgbModal, NgbModalConfig } from '@ng-bootstrap/ng-bootstrap';
import { NgxSpinnerService } from 'ngx-spinner';
import { ToastrService } from 'ngx-toastr';
import Swal from 'sweetalert2';
import { routerTransition } from '../../../../router.animations';
import { ProductManagementService } from '../../product-management.service';
@Component({
    selector: 'app-product-category',
    templateUrl: './product-category.component.html',
    styleUrls: ['./product-category.component.css'],
    animations: [routerTransition()]
})
export class ProductCategoryComponent implements OnInit {
    productCategoryList: any;
    closeResult = '';
    productCategoryForm: FormGroup;
    categoryId: any;

    constructor(
        private productService: ProductManagementService,
        private spinner: NgxSpinnerService,
        config: NgbModalConfig,
        private modalService: NgbModal,
        private fb: FormBuilder,
        private toster: ToastrService,
        private router: Router
    ) {}

    ngOnInit(): void {
        this.allProductCategoryList();
        this.productCategoryForm = this.fb.group({
            categoryId: [0, Validators.required],
            categoryName: ['', Validators.required],
            categoryNameInAr: ['', Validators.required],
            isActive: [true, Validators.required]
        });
    }

    allProductCategoryList() {
        this.spinner.show();
        this.productService.allProductCategoryList().subscribe((res) => {
            this.productCategoryList = res.data.list;
            this.spinner.hide();
            console.log(res);
        });
    }

    open(content, item) {
        this.modalService.open(content, { ariaLabelledBy: 'modal-basic-title' }).result.then(
            (result) => {
                this.closeResult = `Closed with: ${result}`;
            },
            (reason) => {
                this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
            }
        );
    }

    private getDismissReason(reason: any): string {
        if (reason === ModalDismissReasons.ESC) {
            return 'by pressing ESC';
        } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
            return 'by clicking on a backdrop';
        } else {
            return `with: ${reason}`;
        }
    }

    addProductCategory() {
        if (this.productCategoryForm.invalid) {
            return;
        }

        console.log(this.productCategoryForm.value);
        this.spinner.show();
        this.productService.addCategory(this.productCategoryForm.value).subscribe((res) => {
            if (res.status) {
                this.toster.success(res.message);
                this.allProductCategoryList();
                this.modalService.dismissAll();

                this.spinner.hide();
            } else {
                this.spinner.hide();
                this.toster.warning(res.message);
            }
        });
    }

    onDelete(data) {
        this.categoryId = data.categoryId;

        Swal.fire({
            title: 'Delete Request',
            html: 'Are you sure you want to delete this Category',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes',
            cancelButtonText: 'No'
        }).then((result) => {
            if (result.isConfirmed) {
                this.spinner.show();
                this.productService.deleteProductCategory(this.categoryId).subscribe((res) => {
                    this.allProductCategoryList();
                    if (res.status == true) {
                        this.toster.success(res.message);
                    } else {
                        this.toster.warning(res.message);
                    }
                    this.spinner.hide();
                });
            }
        });
    }

    subCategory(row, value) {
        this.categoryId = row.categoryId;
        console.log(this.categoryId);
        if (value === true) {
            this.router.navigate(['/product-management/category', this.categoryId]);
        }
    }
}
