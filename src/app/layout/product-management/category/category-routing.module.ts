import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ProductManagementComponent } from '../product-management.component';
import { ProductCategoryComponent } from './product-category/product-category.component';
import { ProductSubCategoryComponent } from './product-sub-category/product-sub-category.component';
import { ProductSubSubCategoryComponent } from './product-sub-sub-category/product-sub-sub-category.component';

const routes: Routes = [
    {
        path: 'product-management',
        component: ProductManagementComponent,
        children: [
            {
                path: 'category',
                component: ProductCategoryComponent
            },
            {
                path: 'category/:id',
                component: ProductSubCategoryComponent
            },
            ,
            {
                path: 'category/:id/:id1',
                component: ProductSubSubCategoryComponent
            }
        ]
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class CategoryRoutingModule {}
