import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ProductSubSubCategoryComponent } from './product-sub-sub-category.component';

describe('ProductSubSubCategoryComponent', () => {
  let component: ProductSubSubCategoryComponent;
  let fixture: ComponentFixture<ProductSubSubCategoryComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ProductSubSubCategoryComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ProductSubSubCategoryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
