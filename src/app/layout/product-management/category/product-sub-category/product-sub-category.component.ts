import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { NgbModalConfig, NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
import { NgxSpinnerService } from 'ngx-spinner';
import { ToastrService } from 'ngx-toastr';
import Swal from 'sweetalert2';
import { ProductManagementService } from '../../product-management.service';
import { routerTransition } from '../../../../router.animations';

@Component({
    selector: 'app-product-sub-category',
    templateUrl: './product-sub-category.component.html',
    styleUrls: ['./product-sub-category.component.css'],
    animations: [routerTransition()]
})
export class ProductSubCategoryComponent implements OnInit {
    productSubCategoryList: any;
    closeResult = '';
    subCategoryForm: FormGroup;
    categoryId: any;
    subCategoryId: any;

    constructor(
        private productService: ProductManagementService,
        private spinner: NgxSpinnerService,
        config: NgbModalConfig,
        private modalService: NgbModal,
        private fb: FormBuilder,
        private toster: ToastrService,
        private router: Router,
        private route: ActivatedRoute
    ) {}

    ngOnInit(): void {
        this.allSubCategoryList();
        this.subCategoryForm = this.fb.group({
            subCategoryId: [0, Validators.required],
            subCategoryName: ['', Validators.required],
            subCategoryNameInAr: ['', Validators.required],
            isActive: [true, Validators.required]
        });
    }

    allSubCategoryList() {
        this.spinner.show();
        this.categoryId = this.route.snapshot.paramMap.get('id');

        this.productService.getSubCategoryList(this.categoryId).subscribe((res) => {
            this.productSubCategoryList = res.data.list;
            this.spinner.hide();
            console.log(res);
        });
    }

    open(content, item) {
        this.modalService.open(content, { ariaLabelledBy: 'modal-basic-title' }).result.then(
            (result) => {
                this.closeResult = `Closed with: ${result}`;
            },
            (reason) => {
                this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
            }
        );
    }

    private getDismissReason(reason: any): string {
        if (reason === ModalDismissReasons.ESC) {
            return 'by pressing ESC';
        } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
            return 'by clicking on a backdrop';
        } else {
            return `with: ${reason}`;
        }
    }

    addSubCategory() {
        if (this.subCategoryForm.invalid) {
            return;
        }

        var data = {
            ...this.subCategoryForm.value,
            categoryId: this.categoryId
        };
        console.log(data);
        this.spinner.show();
        this.productService.addSubCategory(data).subscribe((res) => {
            if (res.status) {
                this.toster.success(res.message);
                this.allSubCategoryList();
                this.modalService.dismissAll();

                this.spinner.hide();
            } else {
                this.spinner.hide();
                this.toster.warning(res.message);
            }
        });
    }

    onDelete(data) {
        this.subCategoryId = data.subCategoryId;

        Swal.fire({
            title: 'Delete Request',
            html: 'Are you sure you want to delete this Sub-Category',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes',
            cancelButtonText: 'No'
        }).then((result) => {
            if (result.isConfirmed) {
                this.spinner.show();
                this.productService.deleteSubProductCategory(this.subCategoryId).subscribe((res) => {
                    this.allSubCategoryList();
                    if (res.status == true) {
                        this.toster.success(res.message);
                    } else {
                        this.toster.warning(res.message);
                    }
                    this.spinner.hide();
                });
            }
        });
    }

    subCategory(row, value) {
        this.subCategoryId = row.subCategoryId;

        if (value === false) {
            debugger;
            this.router.navigate(['/product-management/category', this.categoryId, this.subCategoryId]);
        }
    }
}
