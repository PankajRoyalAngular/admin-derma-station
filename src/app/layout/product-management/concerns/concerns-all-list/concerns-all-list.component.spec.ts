import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ConcernsAllListComponent } from './concerns-all-list.component';

describe('ConcernsAllListComponent', () => {
  let component: ConcernsAllListComponent;
  let fixture: ComponentFixture<ConcernsAllListComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ConcernsAllListComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ConcernsAllListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
