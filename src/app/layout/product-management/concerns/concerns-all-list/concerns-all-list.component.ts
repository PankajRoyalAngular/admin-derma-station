import { Component, OnInit } from '@angular/core';
import { NgxSpinnerService } from 'ngx-spinner';
import { ToastrService } from 'ngx-toastr';
import { routerTransition } from '../../../../router.animations';
import Swal from 'sweetalert2';
import { ProductManagementService } from '../../product-management.service';
import { ModalDismissReasons, NgbModal, NgbModalConfig } from '@ng-bootstrap/ng-bootstrap';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
    selector: 'app-concerns-all-list',
    templateUrl: './concerns-all-list.component.html',
    styleUrls: ['./concerns-all-list.component.css'],
    animations: [routerTransition()]
})
export class ConcernsAllListComponent implements OnInit {
    concernsList: any;
    concernId: any;
    closeResult = '';
    addConcernForm: FormGroup;
    showId: boolean = false;
    constructor(
        private productService: ProductManagementService,
        private spinner: NgxSpinnerService,
        private toastr: ToastrService,
        private modalService: NgbModal,
        private fb: FormBuilder,
        private toster: ToastrService,
        config: NgbModalConfig
    ) {
        config.backdrop = 'static';
    }

    ngOnInit(): void {
        this.getAllConcerns();
        this.addConcernForm = this.fb.group({
            concernId: [0, Validators.required],
            concernName: ['', Validators.required],
            concernNameInAr: ['', Validators.required],
            showInTopMenu: ['', Validators.required]
        });
    }

    getAllConcerns() {
        this.spinner.show();
        this.productService.allConcernsList().subscribe((res) => {
            this.concernsList = res.data;
            this.spinner.hide();
        });
    }

    onDelete(data) {
        this.concernId = data.concernId;

        Swal.fire({
            title: 'Concerns Request',
            html: 'Are you sure you want to delete this Concerns',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes',
            cancelButtonText: 'No'
        }).then((result) => {
            if (result.isConfirmed) {
                this.spinner.show();
                this.productService.deleteConcerns(this.concernId).subscribe((res) => {
                    this.getAllConcerns();
                    if (res.status == true) {
                        this.toastr.success(res.message);
                    } else {
                        this.toastr.warning(res.message);
                    }
                    this.spinner.hide();
                });
            }
        });
    }

    open(content, item) {
        if (item !== 'add') {
            this.showId = true;
            this.addConcernForm.controls['concernId'].setValue(item.concernId);
            this.addConcernForm.controls['concernName'].setValue(item.concernName);
            this.addConcernForm.controls['concernNameInAr'].setValue(item.concernNameInAr);
            this.addConcernForm.controls['showInTopMenu'].setValue(item.showInTopMenu);
        } else {
            this.addConcernForm.reset();
            this.showId = false;
            this.addConcernForm.controls['concernId'].setValue(0);
        }

        this.modalService.open(content, { ariaLabelledBy: 'modal-basic-title' }).result.then(
            (result) => {
                this.closeResult = `Closed with: ${result}`;
            },
            (reason) => {
                this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
            }
        );
    }

    private getDismissReason(reason: any): string {
        if (reason === ModalDismissReasons.ESC) {
            return 'by pressing ESC';
        } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
            return 'by clicking on a backdrop';
        } else {
            return `with: ${reason}`;
        }
    }

    onAddConcern() {
        if (this.addConcernForm.invalid) {
            return;
        }
        this.spinner.show();
        this.productService.addConcern(this.addConcernForm.value).subscribe((res) => {
            if (res.status) {
                this.toster.success(res.message);
                this.getAllConcerns();
                this.modalService.dismissAll();
                this.addConcernForm.reset();
                this.spinner.hide();
            } else {
                this.spinner.hide();
                this.toster.warning(res.message);
            }
        });
    }
}
