import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ConcernsRoutingModule } from './concerns-routing.module';
import { ConcernsAllListComponent } from './concerns-all-list/concerns-all-list.component';
import { ConcernsComponent } from './concerns.component';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import { PageHeaderModule } from '../../../shared/modules/page-header/page-header.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SharedModule } from '../../../shared/shared.module';

@NgModule({
    declarations: [ConcernsAllListComponent, ConcernsComponent],
    imports: [
        CommonModule,
        ConcernsRoutingModule,
        NgxDatatableModule,
        PageHeaderModule,
        ReactiveFormsModule,
        FormsModule,
        SharedModule
    ]
})
export class ConcernsModule {}
