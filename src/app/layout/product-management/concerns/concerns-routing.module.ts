import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ConcernsAllListComponent } from './concerns-all-list/concerns-all-list.component';
import { ConcernsComponent } from './concerns.component';

const routes: Routes = [
    {
        path: 'product-management',
        component: ConcernsComponent,
        children: [
            {
                path: 'concerns',
                component: ConcernsAllListComponent
            }
        ]
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class ConcernsRoutingModule {}
