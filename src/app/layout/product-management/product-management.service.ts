import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { ApiEndPoint } from '../../shared/enum/api-end-point.enum';
import { environment } from '../../../../src/environments/environment';
import { env } from 'process';

@Injectable({
    providedIn: 'root'
})
export class ProductManagementService {
    constructor(private http: HttpClient) {}
    //brand all list
    allBrandList() {
        return this.http.get<any>(environment.apiUrl + '/' + ApiEndPoint.getAllBrands);
    }

    //Add Brand
    onAddUpdateBrand(data: any) {
        return this.http.post<any>(environment.apiUrl + '/' + ApiEndPoint.addUpdateBrand, data);
    }

    //delete brand
    deleteBrand(data) {
        return this.http.delete<any>(environment.apiUrl + '/' + ApiEndPoint.deleteBrand + '?brandId=' + data);
    }
    //ingredients all list
    allIngredients() {
        return this.http.get<any>(environment.apiUrl + '/' + ApiEndPoint.getAllIngredients);
    }

    //Add Ingredients
    addUpdateIngredients(data: any) {
        return this.http.post<any>(environment.apiUrl + '/' + ApiEndPoint.addUpdateIngredients, data);
    }

    //delete ingredients
    deleteIngredients(data: any) {
        return this.http.delete<any>(environment.apiUrl + '/' + ApiEndPoint.deleteIngrdients + '?ingredientId=' + data);
    }

    //concerns all list
    allConcernsList() {
        return this.http.get<any>(environment.apiUrl + '/' + ApiEndPoint.getAllConcerns);
    }

    //delete concern
    deleteConcerns(data: any) {
        return this.http.delete<any>(environment.apiUrl + '/' + ApiEndPoint.deleteConcern + '?concernId=' + data);
    }

    //Add concern
    addConcern(data: any) {
        return this.http.post<any>(environment.apiUrl + '/' + ApiEndPoint.addConcern, data);
    }

    //All skin Types
    allSkinTypes() {
        return this.http.get<any>(environment.apiUrl + '/' + ApiEndPoint.getAllSkinType);
    }

    //delete skin types
    deleteSkinTypes(data: any) {
        return this.http.delete<any>(environment.apiUrl + '/' + ApiEndPoint.deleteSkinTypes + '?typeId=' + data);
    }

    //Add Skin type
    addSkinType(data: any) {
        return this.http.post<any>(environment.apiUrl + '/' + ApiEndPoint.addSkinType, data);
    }

    //Product-category list
    allProductCategoryList() {
        return this.http.get<any>(environment.apiUrl + '/' + ApiEndPoint.getProductCategoryList);
    }

    //Add product Category
    addCategory(data: any) {
        return this.http.post<any>(environment.apiUrl + '/' + ApiEndPoint.addProductCategory, data);
    }

    //Delete Product Category
    deleteProductCategory(categoryId: any) {
        return this.http.delete<any>(
            environment.apiUrl + '/' + ApiEndPoint.deleteProductCategory + '?categoryId=' + categoryId
        );
    }

    //Sub-Category List
    getSubCategoryList(id: any) {
        return this.http.get<any>(environment.apiUrl + '/' + ApiEndPoint.getSubCategoryList + '?CategoryId=' + id);
    }

    //Add Sub-Category List
    addSubCategory(data: any) {
        return this.http.post<any>(environment.apiUrl + '/' + ApiEndPoint.addSubCategory, data);
    }

    //Delete Sub-Product Category
    deleteSubProductCategory(subCategoryId: any) {
        return this.http.delete<any>(
            environment.apiUrl + '/' + ApiEndPoint.deleteSubCategory + '?subCategoryId=' + subCategoryId
        );
    }
}
