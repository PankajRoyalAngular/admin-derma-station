import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AllTreatmentComponent } from './all-treatment/all-treatment.component';
import { DoctorManagementComponent } from './doctor-management.component';
import { WellnessComponent } from './wellness/wellness.component';

const routes: Routes = [
    {
        path: 'doctor-management',
        component: DoctorManagementComponent,
        children: [
            {
                path: 'treatment',
                component: AllTreatmentComponent
            },
            {
                path: 'wellness',
                component: WellnessComponent
            }
        ]
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class DoctorManagementRoutingModule {}
