import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { ApiEndPoint } from '../../shared/enum/api-end-point.enum';
import { environment } from '../../../environments/environment';
import { map } from 'rxjs/operators';

@Injectable({
    providedIn: 'root'
})
export class DoctorManagementService {
    constructor(private http: HttpClient) {}

    //Treatment List
    allTreatmentList(data: any) {
        return this.http
            .get<any>(
                environment.apiUrl +
                    '/' +
                    ApiEndPoint.getAllTreatmentList +
                    '?pageNumber=' +
                    data.pageNumber +
                    '&pageSize=' +
                    data.pageSize +
                    '&sortOrder=' +
                    data.sortOrder +
                    '&sortField=' +
                    data.sortField +
                    '&searchQuery=' +
                    data.searchQuery +
                    '&filterBy=' +
                    data.filterBy
            )
            .pipe(
                map((res: any) => {
                    return res;
                })
            );
    }

    //Add Update Treatment
    onAddUpdateTreatment(data: any) {
        return this.http.post<any>(environment.apiUrl + '/' + ApiEndPoint.addUpdateTreatment, data);
    }

    //Delete Treatment
    deleteTreatment(treatmentId: any) {
        return this.http.delete<any>(
            environment.apiUrl + '/' + ApiEndPoint.deleteTreatment + '?treatmentId=' + treatmentId
        );
    }

    allWellnessList(data: any) {
        return this.http
            .get<any>(
                environment.apiUrl +
                    '/' +
                    ApiEndPoint.getAllWellness +
                    '?pageNumber=' +
                    data.pageNumber +
                    '&pageSize=' +
                    data.pageSize +
                    '&sortOrder=' +
                    data.sortOrder +
                    '&sortField=' +
                    data.sortField +
                    '&searchQuery=' +
                    data.searchQuery +
                    '&filterBy=' +
                    data.filterBy
            )
            .pipe(
                map((res: any) => {
                    return res;
                })
            );
    }

    //AddUpdate Wellness
    addUpdatWellness(data: any) {
        return this.http.post<any>(environment.apiUrl + '/' + ApiEndPoint.addUpdateWellness, data);
    }

    //Delete Wellness
    deleteWellness(data: any) {
        return this.http.delete<any>(environment.apiUrl + '/' + ApiEndPoint.deleteWellness + '?wellnessId=' + data);
    }
}
