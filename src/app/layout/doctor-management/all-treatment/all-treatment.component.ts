import { Component, OnInit } from '@angular/core';
import { Filter } from '../../../shared/models/filter';
import { routerTransition } from '../../../../app/router.animations';
import { DoctorManagementService } from '../doctor-management.service';
import { NgxSpinnerService } from 'ngx-spinner';
import Swal from 'sweetalert2';
import { ToastrService } from 'ngx-toastr';
import { NgbModal, NgbModalConfig, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
    selector: 'app-all-treatment',
    templateUrl: './all-treatment.component.html',
    styleUrls: ['./all-treatment.component.css'],
    animations: [routerTransition()]
})
export class AllTreatmentComponent implements OnInit {
    treatmentId: any;
    treatmentForm: FormGroup;
    closeResult: any;
    showId: boolean = false;
    filters: Filter = {
        sortOrder: '',
        sortField: '',
        pageNumber: 1,
        pageSize: 10,
        searchQuery: '',
        filterBy: ''
    };
    constructor(
        private doctorService: DoctorManagementService,
        private spinner: NgxSpinnerService,
        private toster: ToastrService,
        private modalService: NgbModal,
        config: NgbModalConfig,
        private fb: FormBuilder
    ) {}
    treatmentList: any;
    ngOnInit(): void {
        this.getAllTreatmentList();
        this.treatmentForm = this.fb.group({
            treatmentId: [0, Validators.required],
            treatmentCategoryId: [0, Validators.required],
            treatmentName: ['', Validators.required],
            treatmentNameInAr: ['', Validators.required]
        });
    }

    getAllTreatmentList() {
        this.spinner.show();
        this.doctorService.allTreatmentList(this.filters).subscribe((res) => {
            this.treatmentList = res.data.dataList;
            this.spinner.hide();
            console.log(this.treatmentList);
        });
    }

    onDelete(data) {
        this.treatmentId = data.treatmentId;

        Swal.fire({
            title: 'Delete Request',
            html: 'Are you sure you want to delete this treatment',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes',
            cancelButtonText: 'No'
        }).then((result) => {
            if (result.isConfirmed) {
                this.spinner.show();
                this.doctorService.deleteTreatment(this.treatmentId).subscribe((res) => {
                    this.getAllTreatmentList();

                    if (res.status == true) {
                        this.toster.success(res.message);
                    } else {
                        this.toster.warning(res.message);
                    }
                    this.spinner.hide();
                });
            }
        });
    }

    open(content, item) {
        if (item !== 'add') {
            this.showId = true;
            this.treatmentForm.controls['treatmentId'].setValue(item.treatmentId);
            this.treatmentForm.controls['treatmentCategoryId'].setValue(item.treatmentCategoryId);
            this.treatmentForm.controls['treatmentName'].setValue(item.treatmentName);
            this.treatmentForm.controls['treatmentNameInAr'].setValue(item.treatmentNameInAr);
        } else {
            this.treatmentForm.reset();
            this.showId = false;
            this.treatmentForm.controls['treatmentId'].setValue(0);
        }

        this.modalService.open(content, { ariaLabelledBy: 'modal-basic-title' }).result.then(
            (result) => {
                this.closeResult = `Closed with: ${result}`;
            },
            (reason) => {
                this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
            }
        );
    }

    private getDismissReason(reason: any): string {
        if (reason === ModalDismissReasons.ESC) {
            return 'by pressing ESC';
        } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
            return 'by clicking on a backdrop';
        } else {
            return `with: ${reason}`;
        }
    }

    addUpdateTreatment() {
        if (this.treatmentForm.invalid) {
            return;
        }
        this.spinner.show();
        this.doctorService.onAddUpdateTreatment(this.treatmentForm.value).subscribe((res) => {
            if (res.status) {
                this.toster.success(res.message);
                this.getAllTreatmentList();
                this.modalService.dismissAll();
                this.treatmentForm.reset();
                this.spinner.hide();
            } else {
                this.spinner.hide();
                this.toster.warning(res.message);
            }
        });
    }
}
