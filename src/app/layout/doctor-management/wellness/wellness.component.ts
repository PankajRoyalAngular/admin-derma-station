import { Component, OnInit } from '@angular/core';
import { Filter } from '../../../../app/shared/models/filter';
import { DoctorManagementService } from '../doctor-management.service';
import { routerTransition } from '../../../../app/router.animations';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { NgbModal, NgbModalConfig, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
import { NgxSpinnerService } from 'ngx-spinner';
import { ToastrService } from 'ngx-toastr';
import Swal from 'sweetalert2';

@Component({
    selector: 'app-wellness',
    templateUrl: './wellness.component.html',
    styleUrls: ['./wellness.component.css'],
    animations: [routerTransition()]
})
export class WellnessComponent implements OnInit {
    wellnessList: any;
    wellnessForm: FormGroup;
    showId: boolean = false;
    closeResult: any;
    wellnessId: any;
    filters: Filter = {
        sortOrder: '',
        sortField: '',
        pageNumber: 1,
        pageSize: 10,
        searchQuery: '',
        filterBy: ''
    };
    constructor(
        private doctorService: DoctorManagementService,
        private spinner: NgxSpinnerService,
        private toastr: ToastrService,
        private modalService: NgbModal,
        config: NgbModalConfig,
        private fb: FormBuilder,
        private toster: ToastrService
    ) {}

    ngOnInit(): void {
        this.getAllWellnessList();
        this.wellnessForm = this.fb.group({
            wellnessId: [0, Validators.required],
            wellnessName: ['', Validators.required],
            wellnessNameInAr: ['', Validators.required]
        });
    }

    getAllWellnessList() {
        this.spinner.show();
        this.doctorService.allWellnessList(this.filters).subscribe((res) => {
            this.wellnessList = res.data.dataList;
            this.spinner.hide();
        });
    }

    open(content, item) {
        if (item !== 'add') {
            this.showId = true;
            this.wellnessForm.controls['wellnessId'].setValue(item.wellnessId);
            this.wellnessForm.controls['wellnessName'].setValue(item.wellnessName);
            this.wellnessForm.controls['wellnessNameInAr'].setValue(item.wellnessNameInAr);
        } else {
            this.wellnessForm.reset();
            this.showId = false;
            this.wellnessForm.controls['wellnessId'].setValue(0);
        }

        this.modalService.open(content, { ariaLabelledBy: 'modal-basic-title' }).result.then(
            (result) => {
                this.closeResult = `Closed with: ${result}`;
            },
            (reason) => {
                this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
            }
        );
    }

    private getDismissReason(reason: any): string {
        if (reason === ModalDismissReasons.ESC) {
            return 'by pressing ESC';
        } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
            return 'by clicking on a backdrop';
        } else {
            return `with: ${reason}`;
        }
    }

    onAddWellness() {
        if (this.wellnessForm.invalid) {
            return;
        }
        this.spinner.show();
        this.doctorService.addUpdatWellness(this.wellnessForm.value).subscribe((res) => {
            if (res.status) {
                this.toster.success(res.message);
                this.getAllWellnessList();
                this.modalService.dismissAll();
                this.wellnessForm.reset();
                this.spinner.hide();
            } else {
                this.spinner.hide();
                this.toster.warning(res.message);
            }
        });
    }

    onDelete(data: any) {
        this.wellnessId = data.wellnessId;

        Swal.fire({
            title: 'Delete Request',
            html: 'Are you sure you want to delete this Wellness',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes',
            cancelButtonText: 'No'
        }).then((result) => {
            if (result.isConfirmed) {
                this.spinner.show();
                this.doctorService.deleteWellness(this.wellnessId).subscribe((res) => {
                    this.getAllWellnessList();

                    if (res.status == true) {
                        this.toster.success(res.message);
                    } else {
                        this.toster.warning(res.message);
                    }
                    this.spinner.hide();
                });
            }
        });
    }
}
