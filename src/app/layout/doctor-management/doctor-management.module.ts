import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { DoctorManagementRoutingModule } from './doctor-management-routing.module';
import { DoctorManagementComponent } from './doctor-management.component';
import { AllTreatmentComponent } from './all-treatment/all-treatment.component';
import { PageHeaderModule } from '../../shared/modules/page-header/page-header.module';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import { WellnessComponent } from './wellness/wellness.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SharedModule } from '../../shared/shared.module';

@NgModule({
    declarations: [DoctorManagementComponent, AllTreatmentComponent, WellnessComponent],
    imports: [
        CommonModule,
        DoctorManagementRoutingModule,
        PageHeaderModule,
        NgxDatatableModule,
        ReactiveFormsModule,
        FormsModule,
        SharedModule
    ]
})
export class DoctorManagementModule {}
