import { HttpClient, HttpClientModule } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { BehaviorSubject, Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { environment } from '../../environments/environment';

import { ApiEndPoint } from '../shared/enum/api-end-point.enum';
import { LoginComponent } from './login/login.component';

@Injectable({
    providedIn: 'root'
})
export class AuthService {
    private currentUserSubject: BehaviorSubject<any>;
    public currentUser: Observable<any>;

    constructor(private http: HttpClient, private router: Router) {
        this.currentUserSubject = new BehaviorSubject<LoginComponent>(JSON.parse(localStorage.getItem('currentUser')));
        this.currentUser = this.currentUserSubject.asObservable();
    }

    public get userValue(): any {
        return this.currentUserSubject.value;
    }
    getToken() {
        return localStorage.getItem('currentUser');
    }
    isLoggedIn() {
        return this.getToken() !== null;
    }

    public get currentUserValue(): any {
        return this.currentUserSubject.value;
    }
    // Admin Login
    login(data: any) {
        return this.http.post<any>(environment.apiUrl + '/' + ApiEndPoint.login, data).pipe(
            map((user) => {
                if (user.status) {
                    localStorage.setItem('currentUser', JSON.stringify(user));
                    this.currentUserSubject.next(user);
                } else {
                    console.log('else part');
                }

                return user;
            })
        );
    }
}
