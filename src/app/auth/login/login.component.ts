import { JsonpClientBackend } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import { Router } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';
import { ToastrService } from 'ngx-toastr';

import { routerTransition } from '../../router.animations';
import { ConstantValues } from '../../shared/enum/constant-values.enum';
import { AuthService } from '../auth.service';

@Component({
    selector: 'app-login',
    templateUrl: './login.component.html',
    styleUrls: ['./login.component.scss'],
    animations: [routerTransition()]
})
export class LoginComponent implements OnInit {
    loginForm: FormGroup;
    submitted = false;
    constructor(
        public router: Router,
        private fb: FormBuilder,
        private authService: AuthService,
        private spinner: NgxSpinnerService,
        private toastr: ToastrService
    ) {}

    ngOnInit() {
        localStorage.clear();
        if (this.authService.currentUser == null) {
            this.router.navigate(['/login']);
        } else {
            this.router.navigate(['/dashboard']);
        }

        this.loginForm = this.fb.group({
            inputValue: ['admin@dermastation.com', [Validators.required, Validators.email]],
            password: [
                'Abs0lve@T3ch',
                [
                    Validators.required,
                    Validators.minLength(8),
                    Validators.pattern('^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-]).{8,}$')
                ]
            ]
            // deviceType: new FormControl(ConstantValues.consDeviceType),
            // deviceToken: new FormControl(ConstantValues.consDeviceToken),
        });
    }

    get f() {
        return this.loginForm.controls;
    }

    onSubmit() {
        this.spinner.show();
        this.submitted = true;
        if (this.loginForm.invalid) {
            this.spinner.hide();

            return;
        }
        var adminLoginDetails = {
            inputValue: 'admin@dermastation.com',
            password: 'Abs0lve@T3ch'
        };

        if (JSON.stringify(adminLoginDetails) === JSON.stringify(this.loginForm.value)) {
            var data = {
                inputValue: this.loginForm.get('inputValue').value,
                password: this.loginForm.get('password').value,
                deviceType: ConstantValues.consDeviceType,
                deviceToken: ConstantValues.consDeviceToken
            };

            this.authService.login(data).subscribe((res) => {
                if (res.status) {
                    this.toastr.success(res.message);
                    localStorage.setItem('accessToken', res.data.accessToken);
                    this.router.navigateByUrl('/dashboard');
                    this.spinner.hide();
                } else {
                    this.spinner.hide();
                    this.toastr.error(res.message);
                }
            });
        } else {
            this.spinner.hide();
            this.toastr.warning('Please enter valid email or password');
            return;
        }
    }
}
