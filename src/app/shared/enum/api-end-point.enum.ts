export enum ApiEndPoint {
    login = 'Auth/AdminLogin',

    //Brand
    getAllBrands = 'Products/GetAllBrands',
    deleteBrand = 'Admin/DeleteBrand',
    addUpdateBrand = 'Admin/AddUpdateBrand',

    //Ingredients
    getAllIngredients = 'Products/GetAllIngredients',
    deleteIngrdients = 'Admin/DeleteIngredient',
    addUpdateIngredients = 'Admin/AddUpdateIngredients',

    //Concerns
    getAllConcerns = 'Products/GetAllConcerns',
    deleteConcern = 'Admin/DeleteConcern',
    addConcern = 'Admin/AddUpdateConcern',

    //Skin
    getAllSkinType = 'Products/GetAllSkinTypes',
    deleteSkinTypes = 'Admin/DeleteSkinType',
    addSkinType = 'Admin/AddUpdateSkinTypes',

    //Treatment
    getAllTreatmentList = 'Admin/GetAllTreatments',
    addUpdateTreatment = 'Admin/AddUpdateTreatments',
    deleteTreatment = 'Admin/DeleteTreatment',

    //Wellness
    getAllWellness = 'Admin/GetAllWellness',
    addUpdateWellness = 'Admin/AddUpdateWellness',
    deleteWellness = 'Admin/DeleteWellness',

    //Blogs
    getBlogsList = 'Blog/GetBlogList',
    getBlogDetails = 'Blog/GetBlogDetail',
    deleteBlog = 'Blog/DeleteBlog',

    //Product-Category
    getProductCategoryList = 'Products/GetProductCategory',
    addProductCategory = 'Products/AddProductCategory',
    deleteProductCategory = 'Products/DeleteProductCategory',

    //Sub-Category
    getSubCategoryList = 'Products/GetProductSubCategory',
    addSubCategory = 'Products/AddProductSubCategory',
    deleteSubCategory = 'Products/DeleteProductSubCategory'
}
