import { Directive, ElementRef, HostListener, Input } from '@angular/core';

@Directive({
    selector: 'input[arbicOnly]'
})
export class OnlyArbicDirective {
    constructor(private _el: ElementRef) {}

    @HostListener('input', ['$event']) onInputChange(event) {
        const initalValue = this._el.nativeElement.value;
        this._el.nativeElement.value = initalValue.replace(/[^أ-ي, ,ء]*/g, '');
        if (initalValue !== this._el.nativeElement.value) {
            event.stopPropagation();
        }
    }
}
