import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { OnlyEnglishDirective } from './directives/only-english.directive';
import { OnlyArbicDirective } from './directives/only-arabic.directive';
import { OnlyNumberDirective } from './directives/only-number.directive';

@NgModule({
    declarations: [OnlyEnglishDirective, OnlyArbicDirective, OnlyNumberDirective],
    imports: [CommonModule],
    exports: [OnlyEnglishDirective, OnlyArbicDirective, OnlyNumberDirective]
})
export class SharedModule {}
