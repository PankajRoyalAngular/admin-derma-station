export interface Filter {
    sortOrder: string;
    sortField: string;
    pageNumber: number;
    pageSize: number;
    searchQuery: string;
    filterBy: string;
}
